terraform {
  backend "s3" {
    bucket = "timestamp"
    key    = "terraform.tfstate"
    region = "eu-west-1"
  }
}
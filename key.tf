resource "aws_key_pair" "mykeypair" {
  key_name   = "mykeypair"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDJVGHssIRhqML0UGQY7q8pQmU9NmFj4AaFQ/7AzeFWeWuvbtBC8vUG3+zaOZmO62RFGACBSlk+TEmvdqyD0VPh9brBTDEff8XNIJVt+okWXjIsORlGSt7X9ZlS4No5j59ZxO8ng+c1/0CuscEhlg1T7s8sZhVAq9EUWEt1VsFWb8GjMi5avAHHyZzE6SdlASc+u03uGPNuLVcRnHYuvF97PK5jgtTpvmqq1djZr+N/nMitsvVQZYUrMQ3TJ59a6ZvJ14kClbyqUtpOqTKUZMZvlS0sec2N+KEnqkxexdChs0NcN0IXID0X6AWhe529iTP0PQ3NViSxo7UBJvkt7avz"
  lifecycle {
    ignore_changes = [public_key]
  }
}

